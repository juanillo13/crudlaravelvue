@extends('layouts.main')

@section('content')
<div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
            <div class="row">
                    <!-- Column -->
                    <div class="col-md-6 col-lg-3">
                        <div class="card card-hover">
                            <div class="box bg-cyan text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-view-dashboard"></i></h1>
                                <h6 class="text-white">Dashboard</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-3">
                        <div class="card card-hover">
                            <div class="box bg-success text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-chart-areaspline"></i></h1>
                                <h6 class="text-white">Charts</h6>
                            </div>
                        </div>
                    </div>
                     <!-- Column -->
                    <div class="col-md-6 col-lg-3">
                        <div class="card card-hover">
                            <div class="box bg-warning text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-collage"></i></h1>
                                <h6 class="text-white">Widgets</h6>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                    <div class="col-md-6 col-lg-3">
                        <div class="card card-hover">
                            <div class="box bg-danger text-center">
                                <h1 class="font-light text-white"><i class="mdi mdi-border-outside"></i></h1>
                                <h6 class="text-white">Tables</h6>
                            </div>
                        </div>
                    </div>
            </div>
            <div id ="crud" class="container-fluid">
                <div   class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body ">    
                            <a href="" class="btn btn-primary" data-toggle="modal" data-target="#create">Nuevo</a>
                            </div>
                            <table class="table">
                                  <thead>
                                    <tr>
                                      <th scope="col">id</th>
                                      <th scope="col">keep</th>
                                      <th scope="col">acción</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                    <tr v-for ="keep in keeps">
                                     
                                      <td> @{{ keep.id }}</td>
                                      <td> @{{ keep.keep }}</td>
                                      <td>
                                          <a href="" class = "btn btn-warning"  v-on:click.prevent="editKeep(keep)">editar</a>
                                          <a href="" class = "btn btn-danger" v-on:click.prevent="deleteKeep(keep)">eliminar</a>
                                    </td>
                                    </tr>
                                   
                                  </tbody>
                            </table>
                           
                            @include('create')
                            @include('edit')
                          
                            
                            
                        </div><!--card-->
                    </div><!--col-12-->
                </div><!--row-->

                
                <div  class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body ">    
                            Datos
                            </div>
                            @{{ $data }}
                        </div>
                    </div>
                </div>
        

</div><!--container-fluid-->
@endsection

