
new Vue({

    el: '#crud',
    //creo esta funcion para traer el metodo getKeeps() cuando se crea mi objeto vue
    created: function(){
        this.getKeeps();
    },
    data:{
        keeps: [],
        newKeep: '',
        fillKeep: {'id': '', 'keep': ''},
        errors: []
    },
    methods:{
        getKeeps: function (){
            var urlKeeps = 'task';
            axios.get(urlKeeps).then(response => {
                this.keeps = response.data
            });
        },
        
        editKeep: function(keep) {
			this.fillKeep.id   = keep.id;
            this.fillKeep.keep = keep.keep;
            
			$('#edit').modal('show');
		},
        updateKeep: function(id){
            var urlEdit = 'task/'+ id;
            console.log(urlEdit);
            axios.put(urlEdit,this.fillKeep).then(response=>{
                this.getKeeps();
                this.fillKeep = {'id':'','keep':''};
                this.errors = [];
                $('#edit').modal('hide');
                toastr.success( 'registro de id: '+ id +' editado correctamente');
            }).catch(error => {
                this.errors = error.response.data
            });
        },
        deleteKeep: function(keep){
            //guardo en una variable la ruta para eleminiar 
            //recordad que esto está en route:list 
            var urlDelete = 'task/' + keep.id;
            //utilizo axios para conectarme con la url
            axios.delete(urlDelete).then(response =>{
                //debo agregar esta funcion para luego de eliminar el registro volver a listar 
                //y se desaparezca el elemento 
                this.getKeeps();
                toastr.success( 'registro de id: '+keep.id+' eliminado correctamente');
            });
        },
        createKeep: function(){
            var urlCreate = 'task';
            axios.post(urlCreate,{
                keep: this.newKeep
            }).then(response=>{
                this.getKeeps();
                this.newKeep = '';
                this.errors = [];
                $('#create').modal('hide');
                toastr.success('Nueva tarea creada con exito');
            }).catch(error => {
                this.errors = error.response.data
            });
        }
    }



});
